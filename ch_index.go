package gocha

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type IndexApiResponse struct {
	Tournament TournamentStructure
}

func refactureResponse(in []IndexApiResponse) (out []TournamentStructure) {
	for _, v := range in {
		out = append(out, v.Tournament)
	}
	return
}

/*
GET tournaments.index

Required

--------

API Key (set in config file)

Optional

--------

state - CH_INDEX_STATE_[STATE] 
	Tour state. (ALL, PENDING, IN_PROGRESS, ENDED)

type - CH_INDEX_TYPE_[TYPE] 
	Tour type. (SINGLE_ELIMINATION, DOUBLE_ELIMINATION, ROUND_ROBIN, SWISS)

created_after - CH_INDEX_CREATED_AFTER
	Supply a date in the format YYYY-MM-DD

created_before - CH_INDEX_CREATED_BEFORE
	Supply a date in the format YYYY-MM-DD

subdomain - CH_INDEX_SUBDOMAIN
	Supply a CHALLONGE! subdomain you've published tournaments in.

*/
func TournamentIndex(data map[string]string) (tours []TournamentStructure, err error) {
	var apiErr APIERROR
	var indResp []IndexApiResponse
	var chIndex []byte
	var resp *http.Response

	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["state"]; a != "" { // Works
			v.Add("state", a)
		}
		if a := data["type"]; a != "" { // Works
			v.Add("type", a)
		}
		if a := data["created_after"]; a != "" { // Works
			v.Add("created_after", a)
		}
		if a := data["created_before"]; a != "" { // Works
			v.Add("created_before", a)
		}
		if a := data["subdomain"]; a != "" { // Works
			v.Add("subdomain", a)
		}
	}
	val := "?" + v.Encode()

	if resp, err = http.Get(API_ROOT + CH_INDEX + val); err != nil {
		return nil, err
	}

	if chIndex, err = ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	}
	json.Unmarshal(chIndex, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return nil, err
	}
	err = json.Unmarshal(chIndex, &indResp)
	if err != nil {
		fmt.Println(err)
	}

	return refactureResponse(indResp), err
}

func ParticipantsIndex(TourUrl string) (Partlist []ParticipantList, err error) {
	var req *http.Request
	var resp *http.Response
	var chPIndex []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	val := "?" + v.Encode()

	if req, err = http.NewRequest("GET", API_ROOT+CH_P_IC1+TourUrl+CH_P_IC2+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chPIndex, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chPIndex, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return nil, err
	}
	err = json.Unmarshal(chPIndex, &Partlist)
	return
}

func MatchesIndex(TourUrl string, data map[string]string) (Matchlist []MatchList, err error) {
	var req *http.Request
	var resp *http.Response
	var chMIndex []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["state"]; a != "" {
			v.Add("state", a)
		}
		if a := data["participant_id"]; a != "" {
			v.Add("participant_id", a)
		}
	}
	val := "?" + v.Encode()

	if req, err = http.NewRequest("GET", API_ROOT+CH_M_IN1+TourUrl+CH_M_IN2+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chMIndex, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chMIndex, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return nil, err
	}
	err = json.Unmarshal(chMIndex, &Matchlist)
	return
}
