package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type ShowApiResponse struct {
	Tournament TournamentStructure
}

/*
GET tournament.show

Required

--------

API Key (set in config file)

Tournament urlname or id - parameter

Optional

--------

include_participants - CH_SHOW_INCLUDE_PARTICIPANTS
	includes participants in returned object (CH_0, CH_1)

include_matches - CH_SHOW_INCLUDE_MATCHES
	includes matches in returned object (CH_0, CH_1)
*/
func TournamentShow(urlname string, data map[string]string) (t TournamentStructure, err error) {
	var resp *http.Response
	var chShow []byte
	var sApiRes ShowApiResponse

	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["include_participants"]; a != "" {
			v.Add("include_participants", a)
		}
		if a := data["include_matches"]; a != "" {
			v.Add("include_matches", a)
		}
	}

	val := "?" + v.Encode()
	if resp, err = http.Get(API_ROOT + CH_SHOW + urlname + ".json" + val); err != nil {
		return
	}

	if chShow, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	err = json.Unmarshal(chShow, &sApiRes)
	t = sApiRes.Tournament
	return
}

type PShowApiResponse struct {
	Participant ParticipantStructure
}

func ParticipantsShow(TourUrl string, PartUrl string, data map[string]string) (Participant ParticipantStructure, err error) {
	var psApiRes PShowApiResponse
	var req *http.Request
	var resp *http.Response
	var chPShow []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["include_matches"]; a != "" {
			v.Add("include_matches", a)
		}
	}
	val := "?" + v.Encode()

	if req, err = http.NewRequest("GET", API_ROOT+CH_P_S1+TourUrl+CH_P_S2+PartUrl+".json"+val, nil); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	if chPShow, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	json.Unmarshal(chPShow, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}

	err = json.Unmarshal(chPShow, &psApiRes)
	Participant = psApiRes.Participant
	return
}

type MShowApiResponse struct {
	Match MatchStructure
}

func MatchesShow(TourUrl string, MatchUrl string) (Match MatchStructure, err error) {
	var req *http.Request
	var resp *http.Response
	var chMShow []byte
	var apiErr APIERROR
	var msApiRes MShowApiResponse

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	val := "?" + v.Encode()

	if req, err = http.NewRequest("GET", API_ROOT+CH_M_SH1+TourUrl+CH_M_SH2+MatchUrl+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chMShow, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chMShow, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}
	err = json.Unmarshal(chMShow, &msApiRes)
	Match = msApiRes.Match
	return
}
