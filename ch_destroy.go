package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type DestroyApiResponse struct {
	Tournament TournamentStructure
}

func TournamentDestroy(TourUrl string) (Tour TournamentStructure, err error) {
	var req *http.Request
	var resp *http.Response
	var chDestroy []byte
	var desApiResp DestroyApiResponse

	client := &http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	val := "?" + v.Encode()
	if req, err = http.NewRequest("DELETE", API_ROOT+CH_DESTROY+TourUrl+".json"+val, nil); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	if chDestroy, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	err = json.Unmarshal(chDestroy, &desApiResp)
	Tour = desApiResp.Tournament
	return
}

type PDestroyApiResponse struct {
	Participant ParticipantStructure
}

func ParticipantsDestroy(TourUrl string, PartUrl string) (Participant ParticipantStructure, err error) {
	var pdApiRes PUpdateApiResponse
	var req *http.Request
	var resp *http.Response
	var chPDestroy []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	val := "?" + v.Encode()

	if req, err = http.NewRequest("DELETE", API_ROOT+CH_P_S1+TourUrl+CH_P_S2+PartUrl+".json"+val, nil); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	if chPDestroy, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	json.Unmarshal(chPDestroy, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}

	err = json.Unmarshal(chPDestroy, &pdApiRes)
	Participant = pdApiRes.Participant
	return
}
