package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

func ParticipantsRandomize(TourUrl string) (Partlist []ParticipantList, err error) {
	var req *http.Request
	var resp *http.Response
	var chPRandomize []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	val := "?" + v.Encode()

	if req, err = http.NewRequest("POST", API_ROOT+CH_RAND+TourUrl+CH_RAND2+".json"+val, nil); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	if chPRandomize, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	json.Unmarshal(chPRandomize, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}

	err = json.Unmarshal(chPRandomize, &Partlist)
	return
}
