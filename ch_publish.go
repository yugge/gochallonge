package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type PublishApiResponse struct {
	Tournament TournamentStructure
}

func TournamentPublish(TourUrl string, data map[string]string) (Tour TournamentStructure, err error) {
	var pubApiRes PublishApiResponse
	var req *http.Request
	var resp *http.Response
	var chPublish []byte

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["include_participants"]; a != "" {
			v.Add("include_participants", a)
		}
	}
	val := "?" + v.Encode()
	if req, err = http.NewRequest("POST", API_ROOT+CH_PUBLISH+TourUrl+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chPublish, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chPublish, &pubApiRes)
	Tour = pubApiRes.Tournament
	return
}
