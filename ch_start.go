package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type StartApiResponse struct {
	Tournament TournamentStructure
}

func TournamentStart(TourUrl string, data map[string]string) (Tour TournamentStructure, err error) {
	var staApiRes StartApiResponse
	var req *http.Request
	var resp *http.Response
	var chStart []byte

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["include_participants"]; a != "" {
			v.Add("include_participants", a)
		}
		if a := data["include_matches"]; a != "" {
			v.Add("include_matches", a)
		}
	}
	val := "?" + v.Encode()
	if req, err = http.NewRequest("POST", API_ROOT+CH_START+TourUrl+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chStart, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chStart, &staApiRes)
	Tour = staApiRes.Tournament
	return
}
