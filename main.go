package gocha

import (
	"errors"
	"time"
)

type APIERROR struct {
	Errors []string
}

type TournamentStructure struct {
	AcceptAttachment          *bool `json:"accept_attachments"`
	AnonVoting                *bool `json:"anonymous_voting"`
	Category                  *string
	CheckInAt                 *time.Time `json:"check_in_at"`
	CompletedAt               *time.Time `json:"completed_at"`
	CreatedAt                 *time.Time `json:"created_at"`
	CreatedByApi              *bool      `json:"created_by_api"`
	CreditCapped              *bool      `json:"credit_capped"`
	GameId                    *bool      `json:"game_id"`
	HideForum                 *bool      `json:"hide_forum"`
	HideSeeds                 *bool      `json:"hide_seeds"`
	HoldThirdPlaceMatch       *bool      `json:"hold_third_place_match"`
	ID                        *int
	Matches                   []MatchList `json:"matches"`
	MaxPredictionsPerUser     *int        `json:"max_predictions_per_user"`
	Name                      *string
	NotifyUsersWhenMatchOpens *bool             `json:"notify_users_when_match_opens"`
	NotifyUsersWhenTourEnds   *bool             `json:"notify_users_when_tournament_ends"`
	OpenSignup                *bool             `json:"open_signup"`
	Participants              []ParticipantList `json:"participants"`
	PredictionMethod          *int              `json:"prediction_method"`
	PredictionsOpenedAt       *time.Time        `json:"predictions_opened_at"`
	Private                   *bool
	ProgressMeter             *int       `json:"progress_meter"`
	PtsForBye                 *float64   `json:"pts_for_bye"`
	PtsForGameTie             *float64   `json:"pts_for_game_tie"`
	PtsForGameWin             *float64   `json:"pts_for_game_win"`
	PtsForMatchTie            *float64   `json:"pts_for_match_tie"`
	PtsForMatchWin            *float64   `json:"pts_for_match_win"`
	PublishedAt               *time.Time `json:"published_at"`
	RankedBy                  *string    `json:"ranked_by"`
	RequireScoreAgreement     *bool      `json:"require_score_agreement"`
	ReviewBeforeFinalizing    *bool      `json:"review_before_finalizing"`
	RoundLabels               *string    `json:"round_labels"` /*Should be yaml*/
	RrPtsForGameTie           *float64   `json:"rr_pts_for_game_tie"`
	RrPtsForGameWin           *float64   `json:"rr_pts_for_game_win"`
	RrPtsForMatchWin          *float64   `json:"rr_pts_for_match_win"`
	RrPtsForMatchTie          *float64   `json:"rr_pts_for_match_tie"`
	SecondPlaceId             *int       `json:"second_place_id"`
	SequentialPairing         *bool      `json:"sequential_pairing"`
	ShowRounds                *bool      `json:"show_rounds"`
	SignupCap                 *bool      `json:"signup_cap"`
	SignupRequiresAccount     *bool      `json:"signup_requires_account"`
	StartedAt                 *time.Time `json:"started_at"`
	SwissRounds               *int       `json:"swiss_rounds"`
	ThirdPlaceId              *int       `json:"third_place_id"`
	TournamentType            *string    `json:"tournament_type"`
	UpdatedAt                 *time.Time `json:"updated_at"`
	Url                       *string
	WinnerId                  *int    `json:"winner_id"`
	DescriptionSource         *string `json:"description_source"` /*Should be HTML*/
	Subdomain                 *string
	FullChallongeUrl          *string `json:"full_challonge_url"`
	LiveImageUrl              *string `json:"live_image_url"`
	SignUpUrl                 *string `json:"sign_up_url"`
}

type ParticipantList struct {
	Participant ParticipantStructure
}

type ParticipantStructure struct {
	Name                 *string
	CheckedIn            *bool   `json:"checked_in"`
	ChallongeEmailAdress *string `json:"challonge_email_adress_verified"`
	Misc                 *string
	Matches              []MatchList `json:"matches"`
	CreatedAt            *time.Time  `json:"created_at"`
	UpdatedAt            *time.Time  `json:"updated_at"`
	FinalRank            *int        `json:"final_rank"`
	ID                   *int        `json:"id"`
	Icon                 *string     `json:"icon"`
	TournamentId         *int        `json:"tournament_id"`
	Seed                 *int        `json:"seed"`
	OnWaitingList        *bool       `json:"on_waiting_list"`
	NewUserEmail         *string     `json:"new_user_email"`
	ChallongeUsername    *string     `json:"challonge_username"`
	Active               *bool       `json:"active"`
}

type MatchList struct {
	Match MatchStructure
}

type MatchStructure struct {
	Player2IsPrereqSeriesLoser *bool      `json:"player2_is_prereq_series_loser"`
	Player1Id                  *int       `json:"player1_id"`
	CreatedAt                  *time.Time `json:"created_at"`
	Player2Id                  *int       `json:"player2_id"`
	UpdatedAt                  *time.Time `json:"updated_at"`
	Round                      *int       `json:"round"`
	ID                         *int       `json:"ID"`
	PrerequisiteMatchIdsCsv    *string    `json:"prerequisite_match_ids_csv"` //CSV
	TournamentId               *int       `json:"tournament_id"`
	Player2PrereqSeriesId      *int       `json:"player2_prereq_series_id"`
	Player1Votes               *int       `json:"player1_votes"`
	WinnerId                   *int       `json:"winner_id"`
	Player2Votes               *int       `json:"player2_votes"`
	Player1PrereqSeriesId      *int       `json:"player1_prereq_series_id"`
	StartedAt                  *time.Time `json:"started_at"`
	HasAttachment              *bool      `json:"has_attachment"`
	LoserId                    *int       `json:"loser_id"`
	Identifier                 *string    `json:"identifier"`
	ScoresCsv                  *string    `json:"scores_csv"` //CSV
	State                      *string    `json:"state"`
	Player1IsPrereqSeriesLoser *bool      `json:"player1_is_prereq_series_loser"`
}

func getApiErrors(e APIERROR) (err error) {
	var errString string

	if e.Errors != nil {
		errString = "API Errors: "
		for _, err := range e.Errors {
			errString += err + ","
		}
		err = errors.New(errString[:len(errString)-1])
	}

	if errString == "API Errors: " {
		return nil
	}

	return

}
