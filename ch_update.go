package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type UpdateApiResponse struct {
	Tournament TournamentStructure
}

func TournamentUpdate(tourURL string, data map[string]string) (tour TournamentStructure, err error) {
	var upApiRes UpdateApiResponse
	var resp *http.Response
	var chUpdate []byte

	client := &http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)

	if data != nil {
		if a := data["subdomain"]; a != "" {
			v.Add("tournament[subdomain]", a)
		}
		if a := data["description"]; a != "" {
			v.Add("tournament[description]", a)
		}
		if a := data["open_signup"]; a != "" {
			v.Add("tournament[open_signup]", a)
		}
		if a := data["hold_third_place_match"]; a != "" {
			v.Add("tournament[hold_third_place_match]", a)
		}
		if a := data["pts_for_match_win"]; a != "" {
			v.Add("tournament[pts_for_match_win]", a)
		}
		if a := data["pts_for_match_tie"]; a != "" {
			v.Add("tournament[pts_for_match_tie]", a)
		}
		if a := data["pts_for_game_tie"]; a != "" {
			v.Add("tournament[pts_for_game_tie]", a)
		}
		if a := data["pts_for_game_win"]; a != "" {
			v.Add("tournament[pts_for_game_win]", a)
		}
		if a := data["pts_for_bye"]; a != "" {
			v.Add("tournament[pts_for_bye]", a)
		}
		if a := data["swiss_rounds"]; a != "" {
			v.Add("tournament[swiss_rounds]", a)
		}
		if a := data["ranked_by"]; a != "" {
			v.Add("tournament[ranked_by]", a)
		}
		if a := data["rr_pts_for_match_win"]; a != "" {
			v.Add("tournament[rr_pts_for_match_win]", a)
		}
		if a := data["rr_pts_for_match_tie"]; a != "" {
			v.Add("tournament[rr_pts_for_match_tie]", a)
		}
		if a := data["rr_pts_for_game_win"]; a != "" {
			v.Add("tournament[rr_pts_for_game_tie]", a)
		}
		if a := data["hide_forum"]; a != "" {
			v.Add("tournament[hide_forum]", a)
		}
		if a := data["show_rounds"]; a != "" {
			v.Add("tournament[show_rounds]", a)
		}
		if a := data["private"]; a != "" {
			v.Add("tournament[private]", a)
		}
		if a := data["notify_users_when_match_open"]; a != "" {
			v.Add("tournament[notify_users_when_match_open]", a)
		}
		if a := data["notify_users_when_the_tournament_ends"]; a != "" {
			v.Add("tournament[notify_users_when_the_tournament_ends]", a)
		}
		if a := data["sequential_pairings"]; a != "" {
			v.Add("tournament[sequential_pairings]", a)
		}
	}
	val := "?" + v.Encode()
	req, err := http.NewRequest("PUT", API_ROOT+CH_UPDATE+tourURL+".json"+val, nil)
	if err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}
	if chUpdate, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	err = json.Unmarshal(chUpdate, &upApiRes)
	tour = upApiRes.Tournament
	return
}

type PUpdateApiResponse struct {
	Participant ParticipantStructure
}

func ParticipantsUpdate(TourUrl string, PartUrl string, data map[string]string) (Participant ParticipantStructure, err error) {
	var puApiRes PUpdateApiResponse
	var req *http.Request
	var resp *http.Response
	var chPUpdate []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["name"]; a != "" {
			v.Add("participant[name]", a)
		}
		if a := data["seed"]; a != "" {
			v.Add("participant[seed]", a)
		}
		if a := data["challonge_username"]; a != "" {
			v.Add("participant[challonge_username]", a)
		}
		if a := data["email"]; a != "" {
			v.Add("participant[email]", a)
		}
		if a := data["misc"]; a != "" {
			v.Add("participant[misc]", a)
		}
	}
	val := "?" + v.Encode()

	if req, err = http.NewRequest("PUT", API_ROOT+CH_P_S1+TourUrl+CH_P_S2+PartUrl+".json"+val, nil); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	if chPUpdate, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	json.Unmarshal(chPUpdate, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}

	err = json.Unmarshal(chPUpdate, &puApiRes)
	Participant = puApiRes.Participant
	return
}

type MUpdateApiResponse struct {
	Match MatchStructure
}

func MatchesUpdate(TourUrl string, MatchUrl string, MatchScoresCSV string, MatchWinnerId string) (Match MatchStructure, err error) {
	var req *http.Request
	var resp *http.Response
	var chMUpdate []byte
	var apiErr APIERROR
	var muApiRes MUpdateApiResponse

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	v.Add("match[scores_csv]", MatchScoresCSV)
	v.Add("match[winner_id]", MatchWinnerId)
	val := "?" + v.Encode()

	if req, err = http.NewRequest("PUT", API_ROOT+CH_M_SH1+TourUrl+CH_M_SH2+MatchUrl+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chMUpdate, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chMUpdate, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}
	err = json.Unmarshal(chMUpdate, &muApiRes)
	Match = muApiRes.Match
	return
}
