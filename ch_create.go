package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type CreateApiResponse struct {
	Tournament TournamentStructure
}

type TournamentType string

/*
POST tournament.create

Required

--------

API Key (set in config file)

Tournament name - Parameter (max 60 chars)

Tournament type - Parameter ( TT_SE,TT_DE,TT_RR,TT_SW )

Tournament URL - Parameter

Optional

--------

subdomain - CH_CREATE_SUBDOMAIN 
	Supply a subdomain string. (string)

description - CH_CREATE_DESCRIPTION
	Supply a tournament description. (string)

open_signup - CH_CREATE_OPEN_SIGNUP
	Have CHALLONGE! host a signup-page: (CH_TRUE,CH_FALSE)

hold_third_place_match - CH_CREATE_HOLD_THIRD_PLACE_MATCH
	Include a match between semifinal losers? (CH_TRUE,CH_FALSE)

pts_for_match_win - CH_CREATE_PTS_FOR_MATCH_WIN
	Points for Match win (swiss only) (string (decimal))

pts_for_match_tie - CH_CREATE_PTS_FOR_MATCH_TIE
	Points for Match tie (swiss only) (string (decimal))

pts_for_game_win - CH_CREATE_PTS_FOR_GAME_WIN
	Points for Game win (swiss only) (string (decimal))

pts_for_game_tie - CH_CREATE_PTS_FOR_GAME_TIE
	Points for Game tie (swiss only) (string (decimal))

pts_for_bye - CH_CREATE_PTS_FOR_BYE
	Points for bye (swiss only) (string (decimal))

swiss_rounds - CH_CREATE_SWISS_ROUNDS
	We recommend limiting the number of rounds to less than two-thirds the number of players. 
	Otherwise, an impossible pairing situation can be reached and your tournament may end 
	before the desired number of rounds are played.  (swiss only) (string (int))

ranked_by - CH_CREATE_RANKED_BY
	How to rank the tournament 
	(CH_CREATE_MATCH_WINS,CH_CREATE_GAME_WINS,CH_CREATE_POINTS_SCORED,CH_CREATE_CUSTOM)

rr_pts_for_match_win - CH_CREATE_RR_PTS_FOR_MATCH_WIN
	Points for match win (round robin only) (string (decimal))

rr_pts_for_match_tie - CH_CREATE_RR_PTS_FOR_MATCH_WIN
	Points for match tie (round robin only) (string (decimal))

rr_pts_for_game_win - CH_CREATE_RR_PTS_FOR_GAME_WIN
	Points for game win (round robin only) (string (decimal))

rr_pts_for_game_tie - CH_CREATE_RR_PTS_FOR_GAME_TIE
	Points for game tie (round robin only) (string (decimal))

accept_attachments - CH_CREATE_ACCEPT_ATTACHMENTS
	Allow match attachment uploads (CH_TRUE,CH_FALSE)

hide_forum - CH_CREATE_HIDE_FORUM
	Hide forums from Challonge! page (CH_TRUE,CH_FALSE)

show_rounds - CH_CREATE_SHOW_ROUNDS
	Label each round above the bracket (CH_TRUE,CH_FALSE)

private - CH_CREATE_PRIVATE
	Hide tournament for the public browsable index and my profile (CH_TRUE,CH_FALSE)

notify_users_when_matches_open - CH_CREATE_NOTIFY_USERS_WHEN_MATCHES_OPEN
	Email registered Challonge! users when matches opens for them (CH_TRUE,CH_FALSE)

notify_users_when_the_tournament_ends - CH_CREATE_NOTIFY_USERS_WHEN_THE_TOURNAMENT_ENDS
	Email registered Challonge! users when tournament ends

sequential_pairings - CH_CREATE_SEQUENTIAL_PAIRINGS
	Instead of traditional seeding rules, make pairing by going straight down the list
	of participants. First round matches are filled in top to bottom, then qualifying matches
	(if applicable) (CH_TRUE,CH_FALSE)

*/
func TournamentCreate(name string, tourtype TournamentType, page string,
	data map[string]string) (newTour TournamentStructure, err error) {
	var apiErr APIERROR
	var cApiRes CreateApiResponse

	v := url.Values{}
	v.Add("api_key", API_KEY)
	if len(name) > 60 {
		v.Add("tournament[name]", name[:60])
	} else {
		v.Add("tournament[name]", name)
	}
	v.Add("tournament[tournament_type]", string(tourtype))
	v.Add("tournament[url]", page)

	if data != nil {
		if a := data["subdomain"]; a != "" {
			v.Add("tournament[subdomain]", a)
		}
		if a := data["description"]; a != "" {
			v.Add("tournament[description]", a)
		}
		if a := data["open_signup"]; a != "" {
			v.Add("tournament[open_signup]", a)
		}
		if a := data["hold_third_place_match"]; a != "" {
			v.Add("tournament[hold_third_place_match]", a)
		}
		if a := data["pts_for_match_win"]; a != "" {
			v.Add("tournament[pts_for_match_win]", a)
		}
		if a := data["pts_for_match_tie"]; a != "" {
			v.Add("tournament[pts_for_match_tie]", a)
		}
		if a := data["pts_for_game_tie"]; a != "" {
			v.Add("tournament[pts_for_game_tie]", a)
		}
		if a := data["pts_for_game_win"]; a != "" {
			v.Add("tournament[pts_for_game_win]", a)
		}
		if a := data["pts_for_bye"]; a != "" {
			v.Add("tournament[pts_for_bye]", a)
		}
		if a := data["swiss_rounds"]; a != "" {
			v.Add("tournament[swiss_rounds]", a)
		}
		if a := data["ranked_by"]; a != "" {
			v.Add("tournament[ranked_by]", a)
		}
		if a := data["rr_pts_for_match_win"]; a != "" {
			v.Add("tournament[rr_pts_for_match_win]", a)
		}
		if a := data["rr_pts_for_match_tie"]; a != "" {
			v.Add("tournament[rr_pts_for_match_tie]", a)
		}
		if a := data["rr_pts_for_game_win"]; a != "" {
			v.Add("tournament[rr_pts_for_game_tie]", a)
		}
		if a := data["hide_forum"]; a != "" {
			v.Add("tournament[hide_forum]", a)
		}
		if a := data["show_rounds"]; a != "" {
			v.Add("tournament[show_rounds]", a)
		}
		if a := data["private"]; a != "" {
			v.Add("tournament[private]", a)
		}
		if a := data["notify_users_when_match_open"]; a != "" {
			v.Add("tournament[notify_users_when_match_open]", a)
		}
		if a := data["notify_users_when_the_tournament_ends"]; a != "" {
			v.Add("tournament[notify_users_when_the_tournament_ends]", a)
		}
		if a := data["sequential_pairings"]; a != "" {
			v.Add("tournament[sequential_pairings]", a)
		}
	}
	resp, err := http.PostForm(API_ROOT+CH_CREATE, v)
	str, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal(str, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}
	err = json.Unmarshal(str, &cApiRes)
	newTour = cApiRes.Tournament
	return
}

type PCreateApiResponse struct {
	Participant ParticipantStructure
}

func ParticipantsCreate(TourUrl string, Name string, data map[string]string) (Participant ParticipantStructure, err error) {
	var pcApiRes PCreateApiResponse
	var req *http.Request
	var resp *http.Response
	var chPCreate []byte
	var apiErr APIERROR

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	v.Add("participant[name]", Name)
	if data != nil {
		if a := data["challonge_username"]; a != "" {
			v.Add("participant[challonge_username]", a)
		}
		if a := data["email"]; a != "" {
			v.Add("participant[email]", a)
		}
		if a := data["seed"]; a != "" {
			v.Add("participant[seed]", a)
		}
		if a := data["misc"]; a != "" {
			v.Add("participant[misc]", a)
		}
	}
	val := "?" + v.Encode()

	if req, err = http.NewRequest("POST", API_ROOT+CH_P_IC1+TourUrl+CH_P_IC2+".json"+val, nil); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	if chPCreate, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	json.Unmarshal(chPCreate, &apiErr)
	if err = getApiErrors(apiErr); err != nil {
		return
	}

	err = json.Unmarshal(chPCreate, &pcApiRes)
	Participant = pcApiRes.Participant
	return
}
