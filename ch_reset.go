package gocha

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type ResetApiResponse struct {
	Tournament TournamentStructure
}

func TournamentReset(TourUrl string, data map[string]string) (Tour TournamentStructure, err error) {
	var resApiRes PublishApiResponse
	var req *http.Request
	var resp *http.Response
	var chReset []byte

	client := http.Client{}
	v := url.Values{}
	v.Add("api_key", API_KEY)
	if data != nil {
		if a := data["include_participants"]; a != "" {
			v.Add("include_participants", a)
		}
	}
	val := "?" + v.Encode()
	if req, err = http.NewRequest("POST", API_ROOT+CH_RESET+TourUrl+".json"+val, nil); err != nil {
		return
	}
	if resp, err = client.Do(req); err != nil {
		return
	}
	if chReset, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}
	json.Unmarshal(chReset, &resApiRes)
	Tour = resApiRes.Tournament
	return
}
